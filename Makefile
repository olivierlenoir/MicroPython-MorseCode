TEXFILE = MicroPythonMorseCode

all: img doc clean

.PHONY: img doc clean

img:
	convert img/Morse_tree.svg img/Morse_tree.png

doc:
	pdflatex ${TEXFILE}.tex
	pdflatex ${TEXFILE}.tex

clean:
	rm ${TEXFILE}.aux
	rm ${TEXFILE}.log
	rm ${TEXFILE}.out
