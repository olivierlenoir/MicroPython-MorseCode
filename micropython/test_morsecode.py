"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2021-04-17 09:57:36
License: MIT, Copyright (c) 2021-2022 Olivier Lenoir
Language: MicroPython v1.18
Project: Test MicroPython Morse Code
Description:
Repository: https://gitlab.com/olivierlenoir/MicroPython-MorseCode
"""


from machine import Pin, PWM
from morsecode import MorseCode


SGN_PIN = const(2)
BUZ_PIN = const(16)

morse = MorseCode(Pin(SGN_PIN, Pin.OUT), PWM(Pin(BUZ_PIN)), wpm=20, vol=2)
morse.send('<CT>')
morse.send('Hello World <AA>')
morse.send('Willkommen! And bienvenue! Welcome! <AA>')
morse.send('<KN> Friend')
morse.send('<AR>')
print(morse.decode('--. --- --- -.. -....- -... -.-- . ...-.-'))
