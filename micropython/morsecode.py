"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2021-03-24
License: MIT, Copyright (c) 2021-2022 Olivier Lenoir
Language: MicroPython V1.18
Project: Morse Code
Link: https://morsecode.world/international/morse.html
Repository: https://gitlab.com/olivierlenoir/MicroPython-MorseCode
"""


# from machine import Pin, PWM
from utime import sleep_ms


class MorseCode:
    """International Morse Code
    1 - The lenght of a dot (.) is one unit.
    2 - A dash (-) is three units.
    3 - The space between parts of the same letter is one unit.
    4 - The space between letters is three units.
    5 - The space between words is seven units."""

    MORSE_CODE = {
        'A': '.-',
        'B': '-...',
        'C': '-.-.',
        'D': '-..',
        'E': '.',
        'F': '..-.',
        'G': '--.',
        'H': '....',
        'I': '..',
        'J': '.---',
        'K': '-.-',
        'L': '.-..',
        'M': '--',
        'N': '-.',
        'O': '---',
        'P': '.--.',
        'Q': '--.-',
        'R': '.-.',
        'S': '...',
        'T': '-',
        'U': '..-',
        'V': '...-',
        'W': '.--',
        'X': '-..-',
        'Y': '-.--',
        'Z': '--..',
        ' ': ' ',
        '1': '.----',
        '2': '..---',
        '3': '...--',
        '4': '....-',
        '5': '.....',
        '6': '-....',
        '7': '--...',
        '8': '---..',
        '9': '----.',
        '0': '-----',
        '&': '.-...',
        "'": '.----.',
        '@': '.--.-.',
        '(': '-.--.',
        ')': '-.--.-',
        ':': '---...',
        ',': '--..--',
        '=': '-...-',
        '!': '-.-.--',
        '.': '.-.-.-',
        '-': '-....-',
        '+': '.-.-.',
        '_': '...-.',
        '"': '.-..-.',
        '?': '..--..',
        '/': '-..-.',
        }
    PROSIGN_CODE = {
        '<AA>': '.-.-',  # New line
        '<AR>': '.-.-.',  # End of message
        '<AS>': '.-...',  # Wait
        '<BK>': '-...-.-',  # Break
        '<BT>': '-...-',  # New paragraph
        '<CL>': '-.-..-..',  # Going off the air (clear)
        '<CT>': '-.-.-',  # Start copying
        '<DO>': '-..---',  # Change to wabun code
        '<KA>': '-.-.-',  # Starting signal
        '<KN>': '-.--.',  # Invite a specific station to transmit
        '<SK>': '...-.-',  # End of transmission
        # '<VA>': '...-.-',  # End of transmission
        '<SN>': '...-.',  # Understood
        # '<VE>': '...-.',  # Understood
        '<SOS>': '...---...',  # Distress message
        }
    MORSE_DECODE = {v: k for k, v in MORSE_CODE.items()}
    PROSIGN_DECODE = {v: k for k, v in PROSIGN_CODE.items()}

    def __init__(self, signal_pin, buzzer_pin):
        """signal_pin as output Pin() object
        buzzer_pin as putput PWM() object
        wpm as Works Per Minute based on PARIS
        freq as PWM buzzer frequency in Hz
        vol as a pseudo volume PWM buzzer duty-cycle"""
        self.signal = signal_pin
        self.buzzer = buzzer_pin
        self._wpm = 20
        self._freq = 550
        self._volume = 32768
        self.reset()

    def send(self, msg):
        """Send message in morse"""
        for word in msg.upper().split():
            self._word(word)
            self.space()

    def receive(self):
        """Receive morse code message"""
        pass

    def dit(self):
        """Dot (.) is one unit"""
        print('.', end='')
        self._ditdah()

    def dah(self):
        """Dash (-) is three units"""
        print('-', end='')
        self._ditdah(3)

    def space(self):
        """Space between words is seven units"""
        print()
        self._unit(6)

    def reset(self):
        """Reset signal and buzzer"""
        self.signal(0)
        self.buzzer.freq(self._freq)
        self.buzzer.duty_u16(0)
        self.delay = self._delay()

    def wpm(self, wpm=None):
        """Set or return WPM"""
        if wpm:
            self._wpm = wpm
            self.reset()
        return self._wpm

    def freq(self, freq=None):
        """Set or return freq"""
        if freq:
            self._freq = freq
            self.reset()
        return self._freq

    def volume(self, volume=None):
        """Set or return volume"""
        if volume:
            self._volume = volume
        return self._volume

    def _delay(self):
        """return unit millisecond delay based on wpm
        PARIS is 50 units per 60000ms -> 1200"""
        return 1200 // self._wpm

    def _unit(self, unit=1):
        """Morse code unit"""
        sleep_ms(self.delay * unit)

    def _ditdah(self, unit=1):
        """Ditdah generator"""
        self.signal(1)
        self.buzzer.duty_u16(self._volume)
        self._unit(unit)
        self.signal(0)
        self.buzzer.duty_u16(0)
        self._unit()

    def _word(self, word):
        """Parse word"""
        if word in self.PROSIGN_CODE:
            self._prosign(word)
        else:
            for letter in word:
                self._letter(letter)

    def _prosign(self, word):
        """Parse prosign"""
        print(word, end=' ')
        self.morse(self.PROSIGN_CODE.get(word, '#'))

    def _letter(self, letter):
        """Parse letter"""
        print(letter, end=' ')
        self.morse(self.MORSE_CODE.get(letter, '#'))

    def morse(self, ditdah):
        """Morse code generator"""
        for dit_dah in ditdah:
            if dit_dah == '.':
                self.dit()
            elif dit_dah == '-':
                self.dah()
            elif dit_dah == ' ':
                self.space()
            else:
                pass
        print()
        self._unit(2)

    def decode(self, ditdah):
        """Decode ditdah message"""
        msg = ''
        for dit_dah in ditdah.split():
            msg += self.MORSE_DECODE.get(dit_dah, self.PROSIGN_DECODE.get(dit_dah, '#'))
        return msg
